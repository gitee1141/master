package com.ming.mq.common.enums;

import lombok.Getter;
import lombok.Setter;

/**
 * @Classname ConsumeEntity
 * @Description TODO
 * @Date 2021/1/13 10:59
 * @Created by yanming.fu
 */
@Getter
@Setter
//@Component自动装配时，如果调用方不加上扫描包配置，是放不进容器去的
public class ConsumeEntity {
    private String applicationName;
    private String topic;
    private String tag;
    private String groupId;
    private String beanName;

    public ConsumeEntity(String applicationName, String topic, String tag, String groupId, String beanName) {
        this.applicationName = applicationName;
        this.topic = topic;
        this.tag = tag;
        this.groupId = groupId;
        this.beanName = beanName;
    }
}
