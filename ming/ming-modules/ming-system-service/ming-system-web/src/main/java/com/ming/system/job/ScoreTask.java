package com.ming.system.job;

import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.handler.annotation.XxlJob;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


@Component
@Slf4j
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class ScoreTask {

    @XxlJob("ScoreTask")
    public ReturnT<String> execute(String param) {
        log.error("=====================定时任务启动=====================");
        for (int i = 0; i < 5; i++) {
            System.out.println(i);
        }
        log.error("=====================定时任务结束===================");
        return ReturnT.SUCCESS;
    }


}
