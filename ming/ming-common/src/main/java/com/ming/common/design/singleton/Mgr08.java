package com.ming.common.design.singleton;

/**
 * 枚举单例(完美中得完美)
 * 不仅仅可以解决线程同步 还能防止反序列化
 */
public enum  Mgr08 {

    INSTANCE;

    public static void main(String[] args) {
        for (int i = 0; i < 100; i++) {
            new Thread(() ->{
                System.out.println(Mgr08.INSTANCE.hashCode());
            }).start();
        }
    }

}
