package com.ming.mq.common.config;

import com.aliyun.openservices.ons.api.MessageListener;
import com.aliyun.openservices.ons.api.bean.Subscription;
import com.aliyun.openservices.shade.com.alibaba.rocketmq.common.filter.ExpressionType;
import com.aliyun.openservices.ons.api.PropertyKeyConst;
import com.ming.common.util.ApplicationContextUtil;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.Properties;

/**
 * @Classname MingMqConfig
 * @Description RockteMq配置类
 * @Date 2021/1/13 11:04
 * @Created by yanming.fu
 */
@EnableConfigurationProperties(MingMqConfig.class)//(去掉下面注解带红色波浪线)
@ConfigurationProperties(prefix ="rocketmq")
@Getter
@Setter
@Configuration
@Component("mingMqConfig2")
public class MingMqConfig{

    @Value("${spring.application.name:}")
    private String applicationName ;

    private String accessKey;
    private String secretKey;
    private String nameSrvAddr;

    public Properties getMqProperty() {
        Properties properties = new Properties();
        properties.setProperty(PropertyKeyConst.AccessKey, this.getAccessKey());
        properties.setProperty(PropertyKeyConst.SecretKey, this.getSecretKey());
        properties.setProperty(PropertyKeyConst.NAMESRV_ADDR, this.getNameSrvAddr());
//        properties.setProperty(PropertyKeyConst.SendMsgTimeoutMillis, "3000");
        return properties;

    }

    public String getApplicationName() {
        return applicationName;
    }


    public static void addSubscription(Map<Subscription, MessageListener> subscriptionMap, String topic, String tagName, String beanName) {
        Subscription ssc = new Subscription();
        ssc.setType(ExpressionType.TAG);
        ssc.setTopic(topic);
        ssc.setExpression(tagName);
        //配置入口类controller的bean名字。
        Object bean = ApplicationContextUtil.getBean(beanName);
        subscriptionMap.put(ssc, (MessageListener) bean);
    }
}
