package com.ming.user;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import tk.mybatis.spring.annotation.MapperScan;
/**
 * @Classname UserApplication
 * @Description TODO
 * @Date 2021/1/5 10:42
 * @Created by yanming.fu
 */
@SpringBootApplication(scanBasePackages = {"com.ming.user","com.ming.common"})
@EnableDiscoveryClient
@EnableCircuitBreaker
@EnableFeignClients("com.ming.user.feign")
@MapperScan("com.ming.user.mapper")
public class UserApplication implements CommandLineRunner {

    private static final Logger logger = LoggerFactory.getLogger(UserApplication.class);

    @Value("${ming.info}")
    private String envs;

    public static void main(String[] args) {
        SpringApplication.run(UserApplication.class, args);
        logger.info("========================【用户服务】启动完毕========================");
    }

    @Override
    public void run(String... args) throws Exception {
        logger.info("========================【{}】========================",envs);
    }
}
