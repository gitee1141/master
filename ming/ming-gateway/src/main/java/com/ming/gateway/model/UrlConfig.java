package com.ming.gateway.model;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import java.util.Set;

/**
 * @Classname UrlConfig
 * @Description 放行URL配置类
 * @Date 2021/2/8 14:03
 * @author  by yanming.fu
 */
@Data
@Configuration
@EnableConfigurationProperties({UrlConfig.class})
@ConfigurationProperties(prefix = ConfigInterface.ConGigName)
public class UrlConfig {

    private Norequire norequire;

    @Data
    public static class  Norequire{

        private Set<String>urls;


    }
}
