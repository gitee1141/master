package com.ming.common.design.singleton;

/**
 * @Classname Mgr02
 * @Description 懒汉式
 * 虽然达到了按需初始化的目的 但会带来线程不安全问题
 *
 * 可以通过synchronized 解决  但是会带来效率会下降
 * @Date 2021/2/22 14:17
 * @Created by yanming.fu
 */
public class Mgr06 {

    private static volatile Mgr06 INSTANCE;

    private Mgr06(){};

    public static synchronized Mgr06 getInstance(){
        if(INSTANCE == null){
            //双重检查  可以实现
            synchronized(Mgr06.class){
                if(INSTANCE==null){
                    try {
                        Thread.sleep(1);
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                    INSTANCE=new Mgr06();
                }

            }
        }
        return INSTANCE;
    }


    public static void main(String[] args) {
        for (int i = 0; i < 100; i++) {
            new Thread(() ->{
                System.out.println(Mgr06.getInstance().hashCode());
            }).start();
        }
    }

}
