package com.ming.common.design.singleton;

/**
 * @Classname Mgr02
 * @Description 懒汉式
 * 虽然达到了按需初始化的目的 但会带来线程不安全问题
 *
 * 可以通过synchronized 解决  但是会带来效率会下降
 * @Date 2021/2/22 14:17
 * @Created by yanming.fu
 */
public class Mgr05 {

    private static Mgr05 INSTANCE;

    private Mgr05(){};

    public static synchronized Mgr05 getInstance(){
        if(INSTANCE == null){
            //妄图通过减小同步代码块的方式提高效率，然后不可行（因为 判断19行  和  21 行不是一体的 中间会存在多线程穿插）
            synchronized(Mgr05.class){
                try {
                    Thread.sleep(1);
                }catch (Exception e){
                    e.printStackTrace();
                }
                INSTANCE=new Mgr05();
            }
        }
        return INSTANCE;
    }


    public static void main(String[] args) {
        for (int i = 0; i < 100; i++) {
            new Thread(() ->{
                System.out.println(Mgr05.getInstance().hashCode());
            }).start();
        }
    }

}
