package com.ming.user.controller;

import com.ming.common.entity.ResultData;
import com.ming.user.LoginResult;
import com.ming.user.User;
import com.ming.user.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @Classname UserController
 * @Description TODO
 * @Date 2021/1/5 13:57
 * @Created by yanming.fu
 */
@RestController
@Api(value = "用户模块",tags={"用户模块"})
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping("/")
    public String index(){
        List<User> user1 = userService.findUser();
        return  user1.toString();
    }

    @ApiOperation("通过feign获取数据")
    @GetMapping("/fegin")
    public String testFeign(){
        ResultData resultData=userService.testFeign();
        return  resultData.toString();
    }


    @ApiOperation("后台管理人员登录")
    @PostMapping("/login")
    public LoginResult login(
            @RequestParam(required = true) String username, // 用户名称
            @RequestParam(required = true) String password  // 用户的密码
    ) {
        return userService.login(username, password);
    }


}
