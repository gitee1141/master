package com.ming.common.util;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @Classname TimeUtil
 * @Description time 工具
 * @Date 2021/1/13 16:15
 * @Created by yanming.fu
 */
public class TimeUtil {

    public static String longToDate(long data){
        Date date = new Date(data);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(date);
    }
}
