package com.ming.user.mq.consume;

import com.alibaba.fastjson.JSON;
import com.aliyun.openservices.ons.api.Action;
import com.aliyun.openservices.ons.api.ConsumeContext;
import com.aliyun.openservices.ons.api.Message;
import com.aliyun.openservices.ons.api.MessageListener;
import com.ming.common.util.TimeUtil;
import com.ming.mq.common.entity.JxStudySource;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * @Classname UserMqConsume
 * @Description TODO
 * @Date 2021/1/13 17:02
 * @Created by yanming.fu
 */
@Slf4j
@Component
public class UserMqConsume implements MessageListener {
    @Override
    public Action consume(Message message, ConsumeContext consumeContext) {
        try {
            JxStudySource kwReq = JSON.parseObject(new String(message.getBody()), JxStudySource.class);
            log.info("接收数据，从rocketMQ->consumer中接收数据：topic：[{}], tag：[{}], key：[{}], bornTime：[{}], 用户属性:[{}], 该消息ID：[{}]， " +
                            "重试消费次数：[{}]， 产生消息主机：[{}]",
                    message.getTopic(), message.getTag(), message.getKey(), TimeUtil.longToDate(message.getBornTimestamp()),
                    message.getUserProperties(), message.getMsgID(), message.getReconsumeTimes(), message.getBornHost());

            log.info("获取到的数据");

            String  buission="ok";
            if (!buission.equals("ok")) {
                return Action.ReconsumeLater;
            }
            return Action.CommitMessage;
        } catch (Exception e) {
            //消费失败
            log.error("消费：{}", e);
            return Action.ReconsumeLater;
        }
    }
}
