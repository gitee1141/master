package com.ming.user;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.util.StringUtils;
import javax.persistence.*;
import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 * @Description 系统菜单
 * @return
 * @date 2021/2/3 14:29
 * @auther yanming.fu
 */
@ApiModel("系统菜单")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name="sys_menu")
public class SysMenu {
    /**
     * 主键
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY,generator = "JDBC")
    @ApiModelProperty("主键")
    private Long id;

    /**
     * 上级菜单ID
     */
    @Column(name = "parent_id")
    @ApiModelProperty("上级菜单ID")
    private Long parentId;

    /**
     * 上级菜单唯一KEY值
     */
    @Column(name = "parent_key")
    @ApiModelProperty("上级菜单唯一KEY值")
    private String parentKey;

    /**
     * 类型 1-分类 2-节点
     */
    @Column(name = "type")
    @ApiModelProperty("类型 1-分类 2-节点")
    private Byte type;

    /**
     * 名称
     */
    @Column(name = "name")
    @ApiModelProperty("名称")
    private String name;

    /**
     * 描述
     */
    @Column(name = "`desc`")
    @ApiModelProperty("描述")
    private String desc;

    /**
     * 目标地址
     */
    @Column(name = "target_url")
    @ApiModelProperty("目标地址")
    private String targetUrl;

    /**
     * 排序索引
     */
    @Column(name = "sort")
    @ApiModelProperty("排序索引")
    private Integer sort;

    /**
     * 状态 0-无效； 1-有效；
     */
    @Column(name = "status")
    @ApiModelProperty("状态 0-无效； 1-有效；")
    private Byte status;

    /**
     * 创建人
     */
    @Column(name = "create_by")
    @ApiModelProperty("创建人")
    private Long createBy;

    /**
     * 修改人
     */
    @Column(name = "modify_by")
    @ApiModelProperty("修改人")
    private Long modifyBy;

    /**
     * 创建时间
     */
    @Column(name = "created")
    @ApiModelProperty("创建时间")
    private Date created;

    /**
     * 修改时间
     */
    @Column(name = "last_update_time")
    @ApiModelProperty("修改时间")
    private Date lastUpdateTime;


    /**
     * 一个菜单对应多个权限
     */
    @ApiModelProperty("该菜单下的所有的权限")
    private List<SysPrivilege> privileges = Collections.emptyList();


    /**
     * 一个菜单对应多个子菜单
     */
    @ApiModelProperty("该菜单的子菜单")
    private List<SysMenu>  childs = Collections.emptyList();


    /**
     * 主要是和前台VUE显示相关
     */
    @ApiModelProperty("该菜单的唯一Key值")
    private  String menuKey ;

    /**
     * 获取菜单的唯一Key凭证
     * @return
     */
    public String getMenuKey() {
        if (!StringUtils.isEmpty(parentKey)) {
            return parentKey+"."+id;
        }else {
            return id.toString();
        }
    }
}