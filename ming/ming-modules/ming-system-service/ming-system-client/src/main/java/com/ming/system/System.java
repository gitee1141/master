package com.ming.system;

import lombok.Builder;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @Classname UserApplication
 * @Description 系统实体类
 * @Date 2021/1/4 17:47
 * @Created by yanming.fu
 */
@Data
@Builder
@Table(name="sys_system")
public class System implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY,generator = "JDBC")
    private String id;
    @Column
    private String name;


}
