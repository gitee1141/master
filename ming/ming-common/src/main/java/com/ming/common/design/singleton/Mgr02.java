package com.ming.common.design.singleton;

/**
 * @Classname Mgr02
 * @Description 饿汉式的 另一种写法
 * @Date 2021/2/22 14:17
 * @Created by yanming.fu
 */
public class Mgr02 {

    private static final  Mgr02 INSTANCE;
    static {
        INSTANCE =new Mgr02();
    }

    private Mgr02(){};

    public static Mgr02 getInstance(){
        return  INSTANCE;
    }


    public static void main(String[] args) {
        Mgr02 m1 = Mgr02.getInstance();
        Mgr02 m2 = Mgr02.getInstance();
        System.out.println(m1==m2);
    }

}
